package com.chennuo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IdeaPartApplication {

    public static void main(String[] args) {
        SpringApplication.run(IdeaPartApplication.class, args);
    }

}
