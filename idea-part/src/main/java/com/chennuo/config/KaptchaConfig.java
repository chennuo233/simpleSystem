package com.chennuo.config;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * Program: idea-part
 * Description:
 * Author: Mr.Chen
 * Create: 2022-03-25 17:49
 **/

@Configuration
public class KaptchaConfig {

    @Bean
    DefaultKaptcha producer(){
        Properties properties = new Properties();
        //边框
        properties.put("kaptcha.border", "yes");
        //颜色
        properties.put("kaptcha.textproducer.font.color", "blue");
        //空行字符
        properties.put("kaptcha.textproducer.char.space", "4");
        //高度
        properties.put("kaptcha.image.height", "40");
        //宽度
        properties.put("kaptcha.image.width", "120");
        //文字大小
        properties.put("kaptcha.textproducer.font.size", "30");

        Config config = new Config(properties);
        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        defaultKaptcha.setConfig(config);

        return defaultKaptcha;
    }
}
