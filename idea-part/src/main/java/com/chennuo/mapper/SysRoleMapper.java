package com.chennuo.mapper;

import com.chennuo.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chennuo252
 * @since 2022-03-25
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
