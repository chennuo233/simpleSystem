package com.chennuo.controller;


import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chennuo.common.dto.SysMenuDto;
import com.chennuo.common.lang.Result;
import com.chennuo.entity.SysMenu;
import com.chennuo.entity.SysRoleMenu;
import com.chennuo.entity.SysUser;
import com.chennuo.service.SysMenuService;
import com.chennuo.service.SysRoleService;
import com.chennuo.service.SysUserService;
import com.chennuo.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chennuo252
 * @since 2022-03-25
 */
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController extends BaseController {

    /**
     * 获取当前用户的菜单或者信息
     * @param principal
     * @return
     */
    //封装了需要返回前端数据的形式！！！！！
    @GetMapping("/nav")
    public Result nav(Principal principal){
        SysUser sysUser = sysUserService.getByUsername(principal.getName());

        //获取权限信息 原本的信息，就是一个一个逗号隔开的
        String authorityInfo = sysUserService.getUserAuthorityInfo(sysUser.getId());

        //将上面的字符转换成数组的形式
        String[] authorityInfoArray = StringUtils.tokenizeToStringArray(authorityInfo, ",");

        //获取导航栏信息

        List<SysMenuDto> navs = sysMenuService.getCurrentUserNav();

        return Result.succ(MapUtil.builder()
                .put("authoritys",authorityInfoArray)
                .put("nav",navs)
                .map());

    }

    //增删改查！！！！！！！！！！
    @GetMapping("/info/{id}")
    @PreAuthorize("hasAuthority('sys:menu:list')")
    public Result info(@PathVariable(name = "id") Long id){
        return Result.succ(sysMenuService.getById(id));
    }

    @GetMapping("/list")
    @PreAuthorize("hasAuthority('sys:menu:list')")
    public Result list(){
        List<SysMenu> menus = sysMenuService.tree();
        return Result.succ(menus);
    }

    @PostMapping("/save")
    @PreAuthorize("hasAuthority('sys:menu:save')")
    public Result save(@Validated @RequestBody SysMenu sysMenu){
        sysMenu.setCreated(LocalDateTime.now());

        sysMenuService.save(sysMenu);

        return Result.succ(sysMenu);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('sys:menu:update')")
    public Result update(@Validated @RequestBody SysMenu sysMenu){
        sysMenu.setUpdated(LocalDateTime.now());

        sysMenuService.updateById(sysMenu);
        //清除所有与该菜单相关的所有权限缓存
        sysUserService.clearUserAuthorityInfoByMenu(sysMenu.getId());

        return Result.succ(sysMenu);
    }

    @PostMapping("/delete/{id}")
    @PreAuthorize("hasAuthority('sys:menu:delete')")
    public Result delete(@PathVariable("id") Long id){
        int count = sysMenuService.count(new QueryWrapper<SysMenu>().eq("parent_id", id));
        if(count > 0){
            return Result.fail("请先删除子菜单");
        }

        //清除所有与该菜单相关的权限缓存
        sysUserService.clearUserAuthorityInfoByMenu(id);

        sysMenuService.removeById(id);

        //同步删除中间关联表
        sysRoleMenuService.remove(new QueryWrapper<SysRoleMenu>().eq("menu_id",id));
        return Result.succ("");
    }

}
