package com.chennuo.controller;

import com.chennuo.common.lang.Result;
import com.chennuo.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Program: idea-part
 * Description:
 * Author: Mr.Chen
 * Create: 2022-03-25 12:48
 **/
@RestController
public class TestController {

    @Autowired
    SysUserService sysUserService;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @PreAuthorize("hasRole('admin')")
    @GetMapping("/test")
    public Result test(){
        return Result.succ(sysUserService.list());
    }

    // 普通用户、超级管理员都可以操作
    @PreAuthorize("hasAuthority('sys:user:list')")
    @GetMapping("/test/pass")
    public Result pass(){

        //加密后的密码：
        String password = bCryptPasswordEncoder.encode("123456");

        boolean matches = bCryptPasswordEncoder.matches("123456", password);

        System.out.println("匹配结果是否正确："+matches);

        return Result.succ(password);
    }

}
