package com.chennuo.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chennuo.common.lang.Const;
import com.chennuo.common.lang.Result;
import com.chennuo.entity.SysRole;
import com.chennuo.entity.SysRoleMenu;
import com.chennuo.entity.SysUserRole;
import com.chennuo.service.SysRoleService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chennuo252
 * @since 2022-03-25
 */
@RestController
@RequestMapping("/sys/role")
public class SysRoleController extends BaseController {

    /**
     *     用户权限列表的增删改查
     * @return
     */

    /**
     * 查找详情，获取详情
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('sys:role:list')")
    @GetMapping("/info/{id}")
    public Result info(@PathVariable("id") Long id){

        SysRole sysRole = sysRoleService.getById(id);
        //获取角色相关联的菜单id
        List<SysRoleMenu> roleMenus = sysRoleMenuService.list(new QueryWrapper<SysRoleMenu>().eq("role_id", id));
        List<Long> menuIds = roleMenus.stream().map(p -> p.getMenuId()).collect(Collectors.toList());

        sysRole.setMenuIds(menuIds);
        return Result.succ(sysRole);
    }

    /**
     * 列表查询,分页查询。。。
     * @param name
     * @return
     */
    @PreAuthorize("hasAuthority('sys:role:list')")
    @GetMapping("/list")
    public Result list(String name){

        Page<SysRole> pageData = sysRoleService.page(getPage(),
                new QueryWrapper<SysRole>()
                        .like(StringUtils.isNotBlank(name), "name", name)
        );

        return Result.succ(pageData);
    }

    /**
     * 添加的方法，用于将数据添加进数据库
     * @param sysRole
     * @return
     */
    @PreAuthorize("hasAuthority('sys:role:save')")
    @PostMapping("/save")
    public Result save(@Validated @RequestBody SysRole sysRole){

        sysRole.setCreated(LocalDateTime.now());
        sysRole.setStatu(Const.STATUS_ON);

        sysRoleService.save(sysRole);
        return Result.succ(sysRole);
    }

    /**
     * 编辑，更新数据
     * @param sysRole
     * @return
     */
    @PreAuthorize("hasAuthority('sys:role:update')")
    @PostMapping("/update")
    public Result update(@Validated @RequestBody SysRole sysRole){

        sysRole.setUpdated(LocalDateTime.now());

        sysRoleService.updateById(sysRole);

        //更新缓存
        sysUserService.clearUserAuthorityInfoByRoleId(sysRole.getId());

        return Result.succ(sysRole);
    }

    /**
     * 删除的方法（批量删除，或者是单个删除同时使用）
     * @param ids
     * @return
     */
    @PreAuthorize("hasAuthority('sys:role:delete')")
    @PostMapping("/delete")
    @Transactional//事务，，，
    public Result delete(@RequestBody Long[] ids){

        sysRoleService.removeByIds(Arrays.asList(ids));

        sysUserRoleService.remove(new QueryWrapper<SysUserRole>().in("role_id",ids));
        sysRoleMenuService.remove(new QueryWrapper<SysRoleMenu>().in("role_id",ids));

        //缓存同步删除，清空，并更新缓存
        Arrays.stream(ids).forEach(id->{
            //更新缓存
            sysUserService.clearUserAuthorityInfoByRoleId(id);
        });

        return Result.succ("删除成功");
    }

    /**
     *
     * @param roleId
     * @param menuIds
     * @return
     */
    @Transactional
    @PostMapping("/perm/{roleId}")
    @PreAuthorize("hasAuthority('sys:role:perm')")
    public Result info(@PathVariable("roleId")Long roleId , @RequestBody Long[] menuIds){
        List<SysRoleMenu> sysRoleMenus = new ArrayList<>();

        Arrays.stream(menuIds).forEach(menuId -> {
            SysRoleMenu roleMenu = new SysRoleMenu();
            roleMenu.setMenuId(menuId);
            roleMenu.setRoleId(roleId);

            sysRoleMenus.add(roleMenu);
        });
        //先删除原来的，才保存新的
        sysRoleMenuService.remove(new QueryWrapper<SysRoleMenu>().eq("role_id",roleId));
        sysRoleMenuService.saveBatch(sysRoleMenus);

        //删除缓存
        sysUserService.clearUserAuthorityInfoByRoleId(roleId);

        return Result.succ(menuIds);
    }
}
