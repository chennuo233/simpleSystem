package com.chennuo.controller;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.map.MapUtil;
import com.chennuo.common.lang.Const;
import com.chennuo.common.lang.Result;
import com.chennuo.entity.SysUser;
import com.chennuo.service.SysUserService;
import com.chennuo.utils.RedisUtil;
import com.google.code.kaptcha.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.Principal;


/**
 * Program: idea-part
 * Description:
 * Author: Mr.Chen
 * Create: 2022-03-25 17:53
 **/

@RestController
public class AuthController {

    @Autowired
    Producer producer;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    SysUserService sysUserService;

    @GetMapping("/captcha")
    public Result captcha() throws IOException {
        //key使用UUID来随机生成
        String key = UUID.randomUUID().toString();
        //code使用写下的redis的构造的信息
        String code = producer.createText();
        //kaptcha提供的将码生成图片的方法

        BufferedImage image = producer.createImage(code);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image,"jpg",outputStream);

        BASE64Encoder encoder = new BASE64Encoder();
        String str = "data:image/jpeg;base64,";

        String base64Img = str + encoder.encode(outputStream.toByteArray());

        redisUtil.hset(Const.CAPTCHA_KEY,key,code,120);

        System.out.println(redisUtil);

        return Result.succ(
                MapUtil.builder()
                        .put("token",key)
                        .put("captchaImg",base64Img)
                        .build()
        );
    }

    /**
     * 获取用户信息接口
     * @param principal
     * @return
     */

    @GetMapping("/sys/userInfo")
    public Result userInfo(Principal principal){

        SysUser sysUser = sysUserService.getByUsername(principal.getName());

        return Result.succ(MapUtil.builder()
                .put("id",sysUser.getId())
                .put("username",sysUser.getUsername())
                .put("avatar",sysUser.getAvatar())
                .put("created",sysUser.getCreated())
                .map());


    }

}
