package com.chennuo.service;

import com.chennuo.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chennuo252
 * @since 2022-03-25
 */
public interface SysUserRoleService extends IService<SysUserRole> {

}
