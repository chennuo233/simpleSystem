package com.chennuo.service.impl;

import com.chennuo.entity.SysUserRole;
import com.chennuo.mapper.SysUserRoleMapper;
import com.chennuo.service.SysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chennuo252
 * @since 2022-03-25
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

}
