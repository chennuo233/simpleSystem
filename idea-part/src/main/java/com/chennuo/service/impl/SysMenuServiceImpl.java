package com.chennuo.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chennuo.common.dto.SysMenuDto;
import com.chennuo.entity.SysMenu;
import com.chennuo.entity.SysUser;
import com.chennuo.mapper.SysMenuMapper;
import com.chennuo.mapper.SysUserMapper;
import com.chennuo.service.SysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chennuo.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chennuo252
 * @since 2022-03-25
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

    @Autowired
    SysUserService sysUserService;

    @Autowired
    SysUserMapper sysUserMapper;

    @Override
    public List<SysMenuDto> getCurrentUserNav() {

        //获取当前用户信息(直接就是？？？？？？)[因为当前用户的信息是直接注入到security里面的，所以可以直接获取]  上下文获取(固定写法)
        String username = (String)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        SysUser sysUser = sysUserService.getByUsername(username);

        List<Long> menuIds = sysUserMapper.getNavMenuIds(sysUser.getId());
        List<SysMenu> menus = this.listByIds(menuIds);

        //转成树状结构
        List<SysMenu> menuTree = buildTreeMenu(menus);

        //实体转DTO
        return covert(menuTree);
    }

    @Override
    public List<SysMenu> tree() {

        //获取所有的菜单信息
        List<SysMenu> sysMenus = this.list(new QueryWrapper<SysMenu>().orderByAsc("orderNum"));
        //进行转换树状结构
        return buildTreeMenu(sysMenus);
    }

    private List<SysMenuDto> covert(List<SysMenu> menuTree) {

        List<SysMenuDto> menuDtos = new ArrayList<>();

        menuTree.forEach(m->{
            SysMenuDto dto = new SysMenuDto();

            dto.setId(m.getId());
            dto.setName(m.getPerms());
            dto.setTitle(m.getName());
            dto.setComponent(m.getComponent());
            dto.setPath(m.getPath());
            dto.setIcon(m.getIcon());

            if(m.getChildren().size()>0){
                //子节点调用当前方法，再次转化
                dto.setChildren(covert(m.getChildren()));
            }

            menuDtos.add(dto);
        });
        return menuDtos;
    }

    //构建树状结构的方法
    private List<SysMenu> buildTreeMenu(List<SysMenu> menus) {
        List<SysMenu> finalMenus = new ArrayList<>();
        //如何获取
        //1、先各自寻找到各自的孩子，
        for(SysMenu menu : menus) {
            for (SysMenu res : menus) {
                if (menu.getId() == res.getParentId()) {
                    menu.getChildren().add(res);
                }
            }
            //2、再提取出父节点
            if(menu.getParentId() == 0L){
                finalMenus.add(menu);
            }
        }
        System.out.println(JSONUtil.toJsonStr(finalMenus));
        return finalMenus;
    }
}
