package com.chennuo.service.impl;

import com.chennuo.entity.SysRoleMenu;
import com.chennuo.mapper.SysRoleMenuMapper;
import com.chennuo.service.SysRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chennuo252
 * @since 2022-03-25
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {

}
