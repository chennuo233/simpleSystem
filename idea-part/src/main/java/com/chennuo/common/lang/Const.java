package com.chennuo.common.lang;

/**
 * Program: idea-part
 * Description:
 * Author: Mr.Chen
 * Create: 2022-03-25 18:06
 **/

public class Const {

    public final static String CAPTCHA_KEY = "captcha";

    public final static Integer STATUS_ON = 0;

    public final static Integer STATUS_OFF = 1;

    public static final CharSequence DEFULT_PASSWORD = "123456";

    public static final String DEFULT_AVATAR =  "https://cube.elemecdn.com/0/88/03b0d39583f48206768a7534e55bcpng.png";
}
