package com.chennuo.common.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * Program: idea-part
 * Description:
 * Author: Mr.Chen
 * Create: 2022-03-26 12:09
 **/

public class CaptchaException extends AuthenticationException {
    public CaptchaException(String msg) {
        super(msg);
    }
}
