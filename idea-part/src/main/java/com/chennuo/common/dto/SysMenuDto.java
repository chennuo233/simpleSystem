package com.chennuo.common.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Program: idea-part
 * Description:   将后端与前端不对应的数据，！！！进行序列化！！！（防止数据库和前后端数据名称的不对应~~~~~）
 * Author: Mr.Chen
 * Create: 2022-03-27 21:21
 **/

/**
*{
*                     name: 'SysUser',
*                     title: '用户管理',
*                     icon: 'el-icon-s-custom',
*                     path: '/sys/users',
*                     component: 'sys/User',
*                     children: []
*                 }
*/
@Data
public class SysMenuDto implements Serializable {

    private Long id;//方便后来的扩展操作，易于查找到menu的实体

    private String name;
    private String title;
    private String icon;
    private String path;
    private String component;
    private List<SysMenuDto> children = new ArrayList<>();

}
