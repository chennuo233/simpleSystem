package com.chennuo.security;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.chennuo.common.exception.CaptchaException;
import com.chennuo.common.lang.Const;
import com.chennuo.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Program: idea-part
 * Description:  一次性的过滤器
 * Author: Mr.Chen
 * Create: 2022-03-26 12:04
 **/
@Component
public class CaptchaFilter extends OncePerRequestFilter {

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    LoginFailureHandler loginFailureHandler;


    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

        String url = httpServletRequest.getRequestURI();

        if("/login".equals(url)&&httpServletRequest.getMethod().equals("POST")){

            //校验验证码
            try {
                validate(httpServletRequest);
            } catch (CaptchaException e) {

                //交给认证失败处理器
                loginFailureHandler.onAuthenticationFailure(httpServletRequest,httpServletResponse,e);
            }

            //如果不正确就跳转到认证失败处理器

        }

        filterChain.doFilter(httpServletRequest,httpServletResponse);

    }

    private void validate(HttpServletRequest httpServletRequest) {

        String code = httpServletRequest.getParameter("code");
        String key = httpServletRequest.getParameter("token");

        if(StringUtils.isBlank(code) || StringUtils.isBlank(key)){
            throw new CaptchaException("验证码错误");
        }

        if(!code.equals(redisUtil.hget(Const.CAPTCHA_KEY,key))){
            throw new CaptchaException("验证码错误");
        }

        //一次性使用
        redisUtil.hdel(Const.CAPTCHA_KEY,key);
    }
}
